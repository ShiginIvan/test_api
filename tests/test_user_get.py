
from lib.assertions import Assertions
from lib.base_case import BaseCase
from lib.my_requests import MyRequests


class TestUserGet(BaseCase):

    def test_get_user_details_not_auth(self):
        id = 2
        response = MyRequests.get(f"user/{id}")
        Assertions.assert_json_has_key(response, "username")
        Assertions.assert_json_has_not_keys(
            response,
            names=["email", "firstName", "lastName"]
            )

    def test_get_user_details_auth_as_same_user(self):
        payload = {
            "email": "vinkotov@example.com",
            "password": "1234"
        }

        response = MyRequests.post("user/login", data=payload)

        auth_sid = self.get_cookie(response, "auth_sid")
        token = self.get_header(response, "x-csrf-token")
        user_id_from_auth_method = self.get_json_value(response, "user_id")

        response_1 = MyRequests.get(
            f"user/{user_id_from_auth_method}",
            headers={"x-csrf-token": token},
            cookies={"auth_sid": auth_sid}
        )
        Assertions.assert_json_has_keys(
            response_1,
            names=["username", "email", "firstName", "lastName"]
        )

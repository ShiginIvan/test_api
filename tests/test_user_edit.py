
from lib.assertions import Assertions
from lib.base_case import BaseCase
from lib.my_requests import MyRequests


class TestUserEdit(BaseCase):
    def test_edit_just_created_user(self):
        payload = self.prepare_registration_data()
        response = MyRequests.post("user/", data=payload)
        Assertions.assert_code_status(response, 200)
        Assertions.assert_json_has_key(response, "id")

        email = payload["email"]
        first_name = payload["firstName"]
        password = payload["password"]
        user_id = self.get_json_value(response, "id")

        #login
        login_data = {
            "email": email,
            "password": password
        }
        response_1 = MyRequests.post("user/login", data=login_data)

        auth_sid = self.get_cookie(response_1, "auth_sid")
        token = self.get_header(response_1, "x-csrf-token")

        #edit
        new_name = "Changed Name"

        response_2 = MyRequests.put(
            f"user/{user_id}",
            headers={"x-csrf-token": token},
            cookies={"auth_sid": auth_sid},
            data={"firstName": new_name}
        )
        Assertions.assert_code_status(response_2, 200)

        #get

        response_3 = MyRequests.get(
                        f"user/{user_id}",
            headers={"x-csrf-token": token},
            cookies={"auth_sid": auth_sid},
        )

        Assertions.assert_json_value_by_name(response_3, "firstName", new_name, "Wrong name of user after edit")
